#!/bin/bash

set -e
cd $(dirname $0)

BASE=$(pwd)
VERSION=0.2.5

rm -rf server/*
cp -a $VERSION/{mods,*.jar,libraries} server/
cp -a config/config server/
# Updated railcraft/buildcraft
rm server/mods/Railcraft*
rm server/mods/buildcraft*
# Computronics is no longer updated, and broken
rm server/mods/Computronics*
# Use Project Red instead of RedLogic.
rm server/mods/redlogic*
# Activate eirairc~
rename 's/.disabled//' server/modpack/eirairc*
# Don't want these!
rm server/mods/ServerTools*
rm server/mods/OpenEye*
# Using newer versions
rm server/mods/eirairc*
rm server/mods/chunkgen*

cp -a extra-mods/* server/mods/

# Use newest dev Eln.
rm server/mods/ElectricalAge*
cd ~/ElectricalAge/
rm -rf build/libs/
./gradlew clean build
cp build/libs/* $BASE/server/mods/
cd $BASE

# Aand prometheus-integration.
cd ~/PrometheusIntegration/
rm -rf build/libs
./gradlew clean build
cp build/libs/* $BASE/server/mods/
cd $BASE

# And copy in "patches".
cp -a patches/* server/config/

# Client?
rm -rf client/*
rsync -a server/ client/
cp -a client-base/* client/

# Naming...
mv server/mods server/modpack
