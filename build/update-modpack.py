#!/usr/bin/env python2.7

import sys
import os
import os.path
import xml.etree.ElementTree as ET
import md5
import urllib
import shutil
import re
import stat
import yaml

urlRoot = 'http://madoka.brage.info/mods/'
dirRoot = '/home/minecraft/web/mods/'


def main():
    # forge = GetForge(open('forge.url').read().strip())
    # return
    global dirRoot
    global urlRoot
    pack = yaml.load(open('pack.txt'))
    packname = pack['packname']
    dirRoot += packname + '/'
    urlRoot += packname + '/'
    pack = Pack('/home/minecraft/web/ServerPack.xml', packname)
    os.unlink('modpack.extract/config.zip')
    os.system('zip -r modpack.extract/config.zip config --exclude config/eirairc.cfg')
    for dirpath, dirnames, filenames in os.walk('modpack'):
        if dirpath != 'modpack':
            continue  # Maybe later.
        for filename in filenames:
            pack.AddOrUpdate(dirpath, filename)
    for dirpath, dirnames, filenames in os.walk('modpack.extract'):
        if dirpath != 'modpack.extract':
            continue
        for filename in filenames:
            pack.AddOrUpdate(dirpath, filename, root=True)
    pack.CleanUnseenMods()
    pack.Write()
    os.system('chmod -R a+r /home/minecraft/web/mods/*')
    for filename in os.listdir('modpack.extract'):
      if filename != 'config.zip' and filename != 'resources.zip':
        os.system('unzip %s' % os.path.join('modpack.extract', filename))


def GetForge(url):
    xml = ET.ElementTree(file=urllib.urlopen(url))



class Pack(object):
    def __init__(self, filename, server):
        self.filename = filename
        self.doc = ET.ElementTree(file=open(filename))
        for s in self.doc.findall('./Server'):
            if s.get('id') == server:
                self.server = s
                break
        assert self.server
        self.server.attrib['revision'] = str(int(self.server.attrib['revision']) + 1)
        self.mods = {}
        self.seen_mods = set()
        for m in self.server.findall('./Module'):
          bare_id = m.find('./BareID').text
          self.mods[bare_id] = m
        self.uploads = []

    def AddOrUpdate(self, dirpath, filename, root=False):
        mod_id = self.FilenameToModId(filename)
        pathname = os.path.join(dirpath, filename)
        md5sum = md5.md5(open(pathname).read()).hexdigest()
        assert mod_id not in self.seen_mods, '%s registered twice' % mod_id
        self.seen_mods.add(mod_id)
        if mod_id in self.mods:
            mod = self.mods[mod_id]
            mod_md5 = mod.findall('./MD5')[0]
            mod.find('./URL').text = urlRoot + urllib.quote(filename)
            if mod_md5.text != md5sum:
                mod_md5.text = md5sum
                # Work around https://github.com/MCUpdater/MCU-API/issues/3
                id_v = re.match(r'.*-([0-9]+)', mod.attrib['id'])
                if id_v:
                  id_version = int(id_v.group(1))
                else:
                  id_version = 0
                id_version += 1
                mod.attrib['id'] = '%s-%d' % (mod_id, id_version)
                self.ScheduleUpload(pathname, filename)
            if not mod.findall('./ModType')[0].attrib.get('inRoot'):
                mod.find('./ModPath').text = os.path.join('mods', filename)
        else:
            print 'Adding new mod', mod_id
            print 'filename:', filename
            mod = ET.SubElement(self.server, 'Module')
            mod.attrib['id'] = mod_id
            mod.attrib['name'] = mod_id
            ET.SubElement(mod, 'URL').text = urlRoot + urllib.quote(filename)
            ET.SubElement(mod, 'Required').text = 'True'
            ET.SubElement(mod, 'MD5').text = md5sum
            ET.SubElement(mod, 'BareID').text = mod_id
            if root:
                tp = ET.SubElement(mod, 'ModType')
                tp.text = 'Extract'
                tp.attrib['inRoot'] = 'true'
            else:
                ET.SubElement(mod, 'ModType').text = 'Regular'
                ET.SubElement(mod, 'ModPath').text = os.path.join('mods', filename)
            self.mods[mod_id] = mod
        self.ScheduleUpload(pathname, filename)

    @staticmethod
    def FilenameToModId(filename):
        mod_id = filename
        patterns = ['[0-9]', '-mc', '-', '_', '-A', '-MC', ' ..?', '-rv', '.jar', '.zip', r'\.']
        patterns = ['.jar', '.zip', r'[-_0-9]*', r'\.+$']
        for pattern in patterns:
            mod_id = re.sub(pattern, '', mod_id)
        return mod_id

    def ScheduleUpload(self, pathname, filename):
        self.uploads.append((pathname, filename))

    def CleanUnseenMods(self):
        for mod_id in set(self.mods).difference(self.seen_mods):
            self.server.remove(self.mods[mod_id])
            del self.mods[mod_id]

    def Write(self):
        # Update serverpack
        Indent(self.doc.getroot())
        self.doc.write('ServerPack.xml.new')
        os.system('diff -u5 %s ServerPack.xml.new' % self.filename)
        print ''
        print 'Press ctrl-c to abort'
        try:
            raw_input()
        except KeyboardInterrupt:
            os.unlink('ServerPack.xml.new')
            sys.exit(1)
        else:
            shutil.copy('ServerPack.xml.new', self.filename)
            os.unlink('ServerPack.xml.new')
            #os.system('s3cmd sync ~/web/ServerPack.xml s3://svein-minecraft')
        # Unpack/copy mods.
        shutil.rmtree('mods')
        os.makedirs('mods')
        for mod in self.mods.itervalues():
            side = mod.attrib.get('side')
            if side != 'CLIENT':
                if not mod.findall('./ModType')[0].attrib.get('inRoot'):
                    filename = mod.findall('./ModPath')[0].text
                    os.link(os.path.join('modpack', os.path.basename(filename)), filename)
        for pathname, filename in self.uploads:
          target = os.path.join(dirRoot, filename)
          shutil.copy(pathname, target)
          os.chmod(pathname, stat.S_IRUSR + stat.S_IWUSR + stat.S_IRGRP + stat.S_IROTH)


def Indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            Indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


if __name__ == '__main__':
    main()

