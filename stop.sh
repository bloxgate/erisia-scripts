#!/bin/bash

cd ~/e1110

source ./scripts.sh

say 'difficulty 0'
sleep 5
say 'difficulty 3'
sleep 10

maybestop() {
  if [[ $(players) = 0 ]]; then
    say save-all
    sleep 5
    say stop
    exit
  fi
}

for grace in `seq 15 -1 0`; do
  maybestop
  say "say Server restarting in $grace minutes, or when empty"
  sleep 60
done

say save-all
sleep 5
say stop
