#!/bin/bash

set -e

say() {
  screen -S e11 -p 0 -X stuff  "$@"`echo -ne '\015'`
}

players() {
    curl -s http://localhost:1234/metrics | grep ^present | wc -l
}

