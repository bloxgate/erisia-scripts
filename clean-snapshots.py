#!/bin/python
#Still needs work - Bloxgate, Nov 10 2015
import commands
import subprocess
import datetime

now = datetime.datetime.now()

zfsList = commands.getoutput("sudo zfs list -t snapshot | grep ^storage/home/minecraft@ | awk '{print $1}'")

day = 0
delete = 0
for zfsSnapshot in reversed(zfsList.split("\n")):
	dateSnapshot = datetime.datetime.strptime(zfsSnapshot.split("@")[1], "%Y-%m-%d_%H%M")
	#elif (delete != 1 ) and ((now - dateSnapshot) <= datetime.timedelta(days = 2)):
	#	print "Would keep-> %s, %s > %s" % (zfsSnapshot, now - dateSnapshot, datetime.timedelta(days = 1))
	#	day -= 1
	if (dateSnapshot.day != day):
		print "Would keep-> %s, New day!" % (zfsSnapshot)
		day = dateSnapshot.day
	elif ((now - dateSnapshot) < datetime.timedelta(days = 1)):
		print "Would keep-> %s, %s < %s" % (zfsSnapshot, now - dateSnapshot, datetime.timedelta(days = 1))
	else:
		print "Would remove-> %s, %s > %s" % (zfsSnapshot, now - dateSnapshot, datetime.timedelta(days = 1))
