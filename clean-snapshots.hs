import Data.Time
import System.Locale
import Control.Arrow
import Data.List
import System.IO.Unsafe
import Debug.Trace
import Data.Maybe

now = unsafePerformIO getCurrentTime

main = interact (unlines . squeeze . lines)


filterPairs :: [a] -> (a -> a -> Bool) -> [a]
filterPairs (x:xs) f = fp x xs
  where
    fp _ [] = []
    fp lastAccepted (next:more)
      | f lastAccepted next = next : fp lastAccepted more
      | otherwise = fp next more

horizon = 86400

-- | Outputs a list of snapshots to remove.
-- 
-- We keep one snapshot per day, and all snapshots younger than a day.
squeeze :: [String] -> [String]
squeeze snaps = map snd squeezed
  where
    timestamped = sort $ map (first fromJust) $ filter (isJust . fst) $ map (parseDate &&& id) snaps
    squeezed = filterPairs timestamped $ \(lastAccepted, _) (cur, _) ->
      case () of
         () | now `diffUTCTime` cur < horizon -> False
         () | cur `diffUTCTime` lastAccepted > horizon -> False
         () | otherwise -> True


parseDate :: String -> Maybe UTCTime
parseDate = parseTime defaultTimeLocale "storage/home/minecraft@%Y-%m-%d_%H%M"
