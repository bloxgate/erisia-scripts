#!/usr/bin/env bash

JAVA=java
JAR=minecraft_server.jar
NAME=erisia

clean_snapshots() {
  sudo /sbin/zfs list -t snapshot \
    | grep ^storage/home/minecraft@ \
    | awk '{print $1}' \
    | runghc clean-snapshots.hs \
    | parallel -j1 --no-notice sudo /sbin/zfs destroy {}
}

#rm -f *.log*

echo -ne "\ek${NAME}\e\\"

(while true; do
   sleep 1740
#   screen -X at $NAME stuff $'save-off\n'
   screen -X at $NAME stuff $'save-all\n'
   sleep 60
   SNAPSHOT=storage/home/minecraft@$(date -u +%Y-%m-%d_%H%M)
   sudo /sbin/zfs snapshot $SNAPSHOT
   sudo /bin/umount /home/minecraft/latest-snapshot
   sudo /bin/mount -t zfs $SNAPSHOT /home/minecraft/latest-snapshot
   clean_snapshots
#   screen -X at $NAME stuff $'save-on\n'
done) &

saver=$!
trap "kill -9 $saver" EXIT

#(
#  sleep 180
#  screen -X at $NAME stuff $'save-off\n'
#) &

$JAVA -d64 -server -Xmn512m -Xms4g -Xmx18g \
  -Djava.net.preferIPv4Stack=true \
  -XX:+AggressiveOpts \
  -XX:+UseG1GC \
  -XX:+DisableExplicitGC -XX:MaxGCPauseMillis=150 -XX:SurvivorRatio=8 \
  -XX:TargetSurvivorRatio=85 -XX:+UseAdaptiveGCBoundary \
  -XX:+StartAttachListener \
  -jar $JAR nogui

javas () {
  ps -u svein | awk '{print $4}' | grep -q java
}

#if javas; then
#  killall java || true
#  sleep 10
#  if javas; then
#    killall -9 java || true
#  fi
#fi
